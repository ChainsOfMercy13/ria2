<?php

namespace App\Http\Controllers;

use App\Emails;
use Illuminate\Http\Request;
use Illuminate\Support\Facade\Input;

class emailController extends Controller
{
    public function index () {

        $emailreturn = Emails::all();

        return view("emaildisplay", compact('emailreturn'));
    }

    public function store (Request $request) {

        $emailSub = new Emails;

        $emailSub->email = Input::get("email");

        $emailSub->save();

        return view("emaildisplay");
        
    }
}
