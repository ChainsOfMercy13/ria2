<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Emails extends Model
{
    protected $connection = 'mysql';
    protected $primaryKey = 'id';
    protected $table = 'emails';
    protected $fillable = ['email'];
}
