@extends("layout.main")

@section("content")

		<!-- Header -->
			<header id="header">
				<h1>Eventually</h1>
				<p>A simple template for telling the world when you'll launch<br />
				your next big thing. Brought to you by <a href="http://html5up.net">HTML5 UP</a>.</p>
			</header>

		<!-- Signup Form -->
			<form id="signup-form" method="POST" action="/emaildisplay">
				<input type="email" name="email" id="email" placeholder="Email Address" />
				{{ csrf_field() }}
				<input type="submit" value="Sign Up" />
			</form>

		<!-- Footer -->
			<footer id="footer">
				<ul class="icons">
					<li><a href="#" class="icon fa-twitter"><span class="label">Twitter</span></a></li>
					<li><a href="#" class="icon fa-instagram"><span class="label">Instagram</span></a></li>
					<li><a href="#" class="icon fa-github"><span class="label">GitHub</span></a></li>
					<li><a href="#" class="icon fa-envelope-o"><span class="label">Email</span></a></li>
				</ul>
				<ul class="copyright">
					<li>&copy; Untitled.</li><li>Credits: <a href="http://html5up.net">HTML5 UP</a></li>
				</ul>
			</footer>
@endsection